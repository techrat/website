---
layout: default
title: Ballarat Bitcoins, Block-chains and Beers
bch: <i class="fa fa-bitcoin"></i>
raffle_address: 1EqdqKqCv3JrFRqdnYcS6R1LwBSNrHNkVm
---

## Meetup Group

The link to our future and past meetups is found here: [Ballarat Bitcoin,
Blockchain and Crypto Currency
Meetup](https://www.meetup.com/Ballarat-Bitcoin-Blockchain-and-Crypto-Currency-Meetup/)

## Trezor Raffle

---

**NOTE:** We are using **Bitcoin Cash** (BCH) for this raffle and **NOT**
Bitcoin due to the currently enormous transaction fees on Bitcoin Core. All
references to {{page.bch}} on this page is for BCH (Bitcoin Cash).

---

For our next meetup we will be raffling off a brand new black [Trezor hardware
wallet](https:/trezor.io) valued at over <code>{{page.bch}}0.35</code>.
Tickets are <code>{{page.bch}}0.01</code> (approximately $3.50) and you can
enter as many times as you like (e.g. sending <code>{{page.bch}}0.05</code> to
the address below would buy you 5 tickets and 5 chances to win).

**BCH** should be sent to <a
href="bitcoincash:{{page.raffle_address}}">`{{page.raffle_address}}`</a> and we
will be running the code found on
[GitHub](https://github.com/scottweston/btc_raffle) on the night to pick our
winner.

<img src="https://chart.googleapis.com/chart?chs=512x512&chld=L|2&cht=qr&chl=bitcoincash:{{page.raffle_address}}?amount=0.02%26label=Trezor%2520Raffle%26message=2%2520Tickets" title="{{page.raffle_address}} for 2 tickets">

You must be at the event to win the Trezor and if we get above
<code>{{page.bch}}0.7</code> worth of entries we will make 2 Trezors available
to win (the same person won't be allowed to win both and we will continue
draws until we get a different winner if by luck the same person comes up
multiple times).

Please download and try our [raffle
code](https://github.com/scottweston/btc_raffle) and be sure you are confident
it is fair, instructions are in the `README.md` file and use the Bitcoin Cash
address `{{page.raffle_address}}`. If you find a bug please submit it via
GitHub or send a pull request.

Once you have bought tickets post the last 6 characters of your sending address
to the meetup event discussion board so that we know who wins on the night. If
you don't post your sending address we will need to see confirmation of the
transfer from your wallet software (which if it's on your desktop might be
difficult ;))

If you need **Bitcoin Cash** to buy tickets please consider using our affiliate
link to create a [CoinSpot](https://www.coinspot.com.au?affiliate=1ECXU)
account. We'll do the draw towards the end of the night so you'll have time to
create an account and buy tickets at the event if you feel uncomfortable
dealing with cryptocurrencies.

If you need a **Bitcoin Cash** wallet check out [Coinomi](https://coinomi.com/)

Good luck!
