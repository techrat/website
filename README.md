# The Tech Ballarat Website Source

https://ballarat.tech

## Building

```
$ git clone git@gitlab.com:techrat/website.git
$ cd website
$ gem install bundler
$ bundle install
$ jekyll serve
```

Most things can be updated via the `_data/` files.

## Updating favicon

```
$ convert img/ballarat_tech_website_ico.svg -define icon:auto-resize=64,48,32,16 favicon.ico
